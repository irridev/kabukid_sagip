package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

//Farm Types in terms of irrigation
const (
	RAIN_FED = iota
	IRRIGATED
)

type Coordinates struct {
	Lat  string `json:"lat"`
	Long string `json:"long"`
}
type WData struct {
	Wkph        float64     `json:"wkph"`
	RFmm        float64     `json:"rfmm"`
	Coordinates Coordinates `json:"coordinates"`
}

type WSData struct {
	PSGC string  `json:"psgc"`
	Data []WData `json:"data"`
}

type WeatherData struct {
	DocType string   `json:"weather_data"`
	WSData  []WSData `json:"wsdata"`
	Date    string   `json:"date"`
}

type InsuranceCoverage struct {
	Amt      string `json:"amt"`
	Currency string `json:"currency"`
}

type Record struct {
	DocType           string            `json:"record"`
	ID                string            `json:"id"`
	Name              string            `json:"name"`
	Country           string            `json:"country"`
	PSGC              string            `json:"region"`
	Address           string            `json:"address"`
	Coordinates       Coordinates       `json:"coordinates"`
	InsuranceCoverage InsuranceCoverage `json:"insurance_coverage"`
	IrrigationType    int               `json:"irrigation_type"`
	RiceType          string            `json:"rice_type"`
	StartDate         string            `json:"start_date"`
	RequiresUpdate    bool              `json:"requires_update"`
}

// SimpleChaincode example simple Chaincode implementation
type SimpleChaincode struct {
}

// ===================================================================================
// Main
// ===================================================================================
func main() {
	err := shim.Start(new(SimpleChaincode))
	if err != nil {
		fmt.Printf("Error starting Simple chaincode: %s", err)
	}
}

// Init initializes chaincode
// ===========================
func (t *SimpleChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

// Invoke - Our entry point for Invocations
// ========================================
func (t *SimpleChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Println("invoke is running " + function)

	// Handle different functions
	switch function {
	case "init_records":
		return t.InitRecords(stub, args)
	case "new_weather_data":
		return t.NewWeatherData(stub, args)
	default:
		fmt.Println("invoke did not find func: " + function)
		return shim.Error("Received unknown function invocation")
	}
}

//InitRecords init data
func (t *SimpleChaincode) InitRecords(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	rec := Record{
		DocType: "Record"
		ID:      "1QAZ2WSX3EDC",
		Name:    "Vaun Eros Farm",
		Country: "Philippines",
		PSGC:    "050500000",
		Address: "Camalig, Albay",
		Coordinates: Coordinates{
			Lat:  "13.1218006",
			Long: "123.6320573",
		},
		InsuranceCoverage: InsuranceCoverage{
			Amt:      "2800000",
			Currency: "PHP",
		},
		IrrigationType: RAIN_FED,
		RiceType:       "IR64",
		StartDate:      "2019-08-01",
		RequiresUpdate: false,
	}

	log.Printf("[InitRecords]...record for initialization:\n[+%v]", rec)
	recAsBytes, err := json.Marshal(rec)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(rec.ID, recAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

//NewWeatherData
func (t *SimpleChaincode) NewWeatherData(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	//  0-message
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	// ==== Input sanitation ====\
	if len(args[0]) == 0 {
		return shim.Error("1st argument must be a non-empty string")
	}

	wsdata := WeatherData{}
	err := json.Unmarshal([]byte(args[0]), &wsdata)
	if err != nil {
		log.Printf("[NewWeatherData]...error! unable to unmarshall message \n[+%v]", args[0])
		return shim.Error("[NewWeatherData]...error! unable to unmarshall message")
	}

	wdAsBytes, err := json.Marshal(wsdata)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(wsdata.Date, wdAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.SetEvent("new_weather_data_recorded", []byte(wsdata.Date))
	if err != nil {
		fmt.Println("unable to send event")
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

func (t *SimpleChaincode) AssessFarms(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	// x
	// "date-string"
	// "queryString"
	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	farmsAsBytes, err := getQueryResultForQueryString(stub, args[1])
	if err != nil {
		return shim.Error(err.Error())
	}

	wsdata := &WeatherData{}
	wsdataAsBytes, err := stub.GetState(args[2])
	err = json.Unmarshal(wsdataAsBytes, &wsdata)
	if err != nil {
		log.Printf("[AssessFarms]...error! unable to unmarshall message \n[+%v]", args[0])
		return shim.Error("[AssessFarms]...error! unable to unmarshall message")
	}

	farms := []Record{}
	err = json.Unmarshal(farmsAsBytes, &farms)
	if err != nil {
		log.Printf("[AssessFarms]...error! unable to unmarshall message \n[+%v]", args[0])
		return shim.Error("[AssessFarms]...error! unable to unmarshall message")
	}
	farmsForClaimSettlement := Assess(wsdata, farms)
	fcsAsBytes, err := json.Marshal(farmsForClaimSettlement)
	if err != nil {
		return shim.Error(err.Error())
	}

	if len(farms) != 0 {
		err = stub.PutState(wsdata.Date+"_settlement_claims", fcsAsBytes)
		if err != nil {
			return shim.Error(err.Error())
		}

		err = stub.SetEvent("farms_for_settlement_claims", fcsAsBytes)
		if err != nil {
			fmt.Println("unable to send event")
			return shim.Error(err.Error())
		}
	}

	return shim.Success(nil)
}

/*
	Number of days IR64 (7mm/day - FAO, 110 days)
	Vegetative 45 - 315
	Flowering 35 - 245
	Maturity 30 - 210

	PSBRc82 (7mm/day - FAO, 120 days)
	Vegetative 55 - 385
	Flowering 35 - 245
	Maturity 30 - 210

	fix upper threshold at the ff:
		vegetative -
*/

func Assess(wsdata *WeatherData, farmRecords []Record) []Record {
	currDate := time.Now()
	affectedFarms := []Record{}
	for _, data := range wsdata.WSData {
		for _, farm := range farmRecords {
			if farm.PSGC == data.PSGC {
				for _, location := range data.Data {
					farmStartDate, _ := time.Parse(farm.StartDate, farm.StartDate)
					diff := currDate.Sub(farmStartDate)
					if (location.Wkph > 120) && (location.RFmm > 30) && diff > 15 {
						affectedFarms = append(affectedFarms, farm)
					}
				}
			}
		}
	}

	return affectedFarms
}

// =========================================================================================
// getQueryResultForQueryString executes the passed in query string.
// Result set is built and returned as a byte array containing the JSON results.
// =========================================================================================
func getQueryResultForQueryString(stub shim.ChaincodeStubInterface, queryString string) ([]byte, error) {

	fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)
	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryRecords
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())

	return buffer.Bytes(), nil
}
