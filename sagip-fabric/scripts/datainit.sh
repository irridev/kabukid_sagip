#!/bin/bash
export CHANNEL_NAME=channela
export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/kabukid.org/orderers/orderer.kabukid.org/msp/tlscacerts/tlsca.kabukid.org-cert.pem
export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ic.org/users/Admin@ic.org/msp
export CORE_PEER_ADDRESS=peer0.wdp.org:7051
export CORE_PEER_LOCALMSPID="wdpMSP"
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ic.org/peers/peer0.wdp.org/tls/ca.crt

#type Message struct {
#	CID      string  `json:"CID"`  //gateway will generate
#	Type     string  `json:"Type"` //UI message starts here
#	Username string  `json:"Username"`
#	OrgCode  string  `json:"OrgCode"`
#	Payload  Payload `json:"Payload"`
#}

#type FPInfo struct {
#	FPName       string `json:"FPName"`
#	FPCode       string `json:"FPCode"`
#	FPProgram    string `json:"FPProgram"`
#	CurrInvstAmt string `json:"CurrInvstAmt,omitempty"`
#	Currency     string `json:"Currency,omitempty"`
#}

peer chaincode invoke -n irfpcc -c '{"Args":["data_init_FPA", ""]}' -C $CHANNEL_NAME --tls --cafile $ORDERER_CA

#peer chaincode invoke -n irfpcc -c '{"Args":["enrollment_request", "{\"ClientID\": \"1QAZ2WSX3EDC\",\"ClientType\": \"employer\",\"Data\":\"{\\\"FPData\\\":\\\"{\\\\\\\"Name\\\\\\\": \\\\\\\"FPA\\\\\\\",\\\\\\\"FPCode\\\\\\\": \\\\\\\"FPA\\\\\\\"}\\\",\\\"EmpData\\\":\\\"{\\\\\\\"ID\\\\\\\": \\\\\\\"0OKM9IJN8\\\\\\\",\\\\\\\"FristName\\\\\\\": \\\\\\\"Manong\\\\\\\",\\\\\\\"MiddleName\\\\\\\": \\\\\\\"Karding\\\\\\\",\\\\\\\"LastName\\\\\\\": \\\\\\\"jr\\\\\\\",\\\\\\\"Birthday\\\\\\\": \\\\\\\"1989-09-19\\\\\\\",\\\\\\\"Age\\\\\\\": \\\\\\\"29\\\\\\\",\\\\\\\"Address\\\\\\\": \\\\\\\"talipapa st\\\\\\\",\\\\\\\"Email\\\\\\\": \\\\\\\"karding13@gmail.com\\\\\\\",\\\\\\\"ContactNo\\\\\\\": \\\\\\\"+63911123456\\\\\\\",\\\\\\\"TaxID\\\\\\\": \\\\\\\"1123456789000\\\\\\\",\\\\\\\"Employer\\\\\\\": \\\\\\\"XYZ Inc.\\\\\\\"}\\\"}\"}"]}' -C $CHANNEL_NAME --tls --cafile $ORDERER_CA
#peer chaincode invoke -n irfpcc -c '{"Args":["enrollment_request", "{\"ClientID\": \"1QAZ2WSX3EDC\",\"ClientType\": \"employer\",\"Data\":\"{\\\"FPData\\\":\\\"{\\\\\\\"Name\\\\\\\": \\\\\\\"FPA\\\\\\\",\\\\\\\"FPCode\\\\\\\": \\\\\\\"FPA\\\\\\\"}\\\",\\\"EmpData\\\":\\\"{\\\\\\\"ID\\\\\\\": \\\\\\\"8UHB7YGV6\\\\\\\",\\\\\\\"FristName\\\\\\\": \\\\\\\"Manong\\\\\\\",\\\\\\\"MiddleName\\\\\\\": \\\\\\\"Karding\\\\\\\",\\\\\\\"LastName\\\\\\\": \\\\\\\"jr\\\\\\\",\\\\\\\"Birthday\\\\\\\": \\\\\\\"1989-09-19\\\\\\\",\\\\\\\"Age\\\\\\\": \\\\\\\"29\\\\\\\",\\\\\\\"Address\\\\\\\": \\\\\\\"talipapa st\\\\\\\",\\\\\\\"Email\\\\\\\": \\\\\\\"karding13@gmail.com\\\\\\\",\\\\\\\"ContactNo\\\\\\\": \\\\\\\"+63911123456\\\\\\\",\\\\\\\"TaxID\\\\\\\": \\\\\\\"1123456789000\\\\\\\",\\\\\\\"Employer\\\\\\\": \\\\\\\"XYZ Inc.\\\\\\\"}\\\"}\"}"]}' -C $CHANNEL_NAME --tls --cafile $ORDERER_CA