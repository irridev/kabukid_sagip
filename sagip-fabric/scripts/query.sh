#!/bin/bash
export CHANNEL_NAME=channela
export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/kabukid.org/orderers/orderer.kabukid.org/msp/tlscacerts/tlsca.kabukid.org-cert.pem
export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ic.org/users/Admin@ic.org/msp
export CORE_PEER_ADDRESS=peer0.wdp.org:7051 
export CORE_PEER_LOCALMSPID="wdpMSP"
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ic.org/peers/peer0.wdp.org/tls/ca.crt

#peer chaincode query -C mychannel -n marblesp -c '{"Args":["queryMarbles","{\"selector\":{\"docType\":\"marble\",\"owner\":\"tom\"}, \"use_index\":[\"_design/indexOwnerDoc\", \"indexOwner\"]}"]}'
peer chaincode query -C $CHANNEL_NAME --tls --cafile $ORDERER_CA -n irfpcc -c '{"Args":["records_list_request","{\"selector\":{\"ID\":\"1QAZ2WSX3EDC\"}, \"use_index\":[\"_design/indexID\", \"indexID\"]}"]}'