#!/bin/bash

export CHANNEL_NAME=channela
export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/kabukid.org/orderers/orderer.kabukid.org/msp/tlscacerts/tlsca.kabukid.org-cert.pem
export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ic.org/users/Admin@ic.org/msp
export CORE_PEER_ADDRESS=peer0.wdp.org:7051 
export CORE_PEER_LOCALMSPID="wdpMSP"
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ic.org/peers/peer0.wdp.org/tls/ca.crt
export VER=1.2

#install chaincode to peers
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ic.org/users/Admin@ic.org/msp CORE_PEER_ADDRESS=peer0.wdp.org:7051 CORE_PEER_LOCALMSPID="wdpMSP" CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ic.org/peers/peer0.wdp.org/tls/ca.crt peer chaincode install -n irfpcc -v $VER -p chaincode/irfpcc
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ic.org/users/Admin@ic.org/msp CORE_PEER_ADDRESS=peer0.ic.org:7051 CORE_PEER_LOCALMSPID="icMSP" CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ic.org/peers/peer0.ic.org/tls/ca.crt peer chaincode install -n irfpcc -v $VER -p chaincode/irfpcc
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank.org/users/Admin@bank.org/msp CORE_PEER_ADDRESS=peer0.bank.org:7051 CORE_PEER_LOCALMSPID="bankMSP" CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank.org/peers/peer0.bank.org/tls/ca.crt peer chaincode install -n irfpcc -v $VER -p chaincode/irfpcc

peer chaincode upgrade -o orderer.kabukid.org:7050 --tls --cafile $ORDERER_CA -C $CHANNEL_NAME -n irfpcc -v $VER -c '{"Args":[""]}' -P "OutOf(2, 'wdpMSP.member', 'icMSP.member', 'bankMSP.member')" --collections-config /opt/gopath/src/chaincode/irfpcc/collections_config.json
