#!/bin/bash

# Environment variables for PEER0
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/kabukid.org/users/Admin@kabukid.org/msp
CORE_PEER_ADDRESS=peer0.kabukid.org:7051
CORE_PEER_LOCALMSPID="kabukidMSP"
CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/kabukid.org/peers/peer0.kabukid.org/tls/ca.crt


export CHANNEL_NAME=channela
export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/kabukid.org/orderers/orderer.kabukid.org/msp/tlscacerts/tlsca.kabukid.org-cert.pem

#peer channel create -o orderer.kabukid.org:7050 -c $CHANNEL_NAME -f ./channel-artifacts/$CHANNEL_NAME.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem

#create channel block
peer channel create -o orderer.kabukid.org:7050 -c $CHANNEL_NAME -f ./channel-artifacts/$CHANNEL_NAME.tx --tls --cafile $ORDERER_CA

#join peers to channel
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/wdp.org/users/Admin@wdp.org/msp CORE_PEER_ADDRESS=peer0.wdp.org:7051 CORE_PEER_LOCALMSPID="wdpMSP" CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/wdp.org/peers/peer0.wdp.org/tls/ca.crt peer channel join -b $CHANNEL_NAME.block
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ic.org/users/Admin@ic.org/msp CORE_PEER_ADDRESS=peer0.ic.org:7051 CORE_PEER_LOCALMSPID="icMSP" CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ic.org/peers/peer0.ic.org/tls/ca.crt peer channel join -b $CHANNEL_NAME.block
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank.org/users/Admin@bank.org/msp CORE_PEER_ADDRESS=peer0.bank.org:7051 CORE_PEER_LOCALMSPID="bankMSP" CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank.org/peers/peer0.bank.org/tls/ca.crt peer channel join -b $CHANNEL_NAME.block
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/kabukid.org/users/Admin@kabukid.org/msp CORE_PEER_ADDRESS=peer0.kabukid.org:7051 CORE_PEER_LOCALMSPID="kabukidMSP" CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/kabukid.org/peers/peer0.kabukid.org/tls/ca.crt peer channel join -b $CHANNEL_NAME.block
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/lgu.org/users/Admin@lgu.org/msp CORE_PEER_ADDRESS=peer0.lgu.org:7051 CORE_PEER_LOCALMSPID="lguMSP" CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/lgu.org/peers/peer0.lgu.org/tls/ca.crt peer channel join -b $CHANNEL_NAME.block

#try to update peer
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ic.org/users/Admin@ic.org/msp CORE_PEER_ADDRESS=peer0.wdp.org:7051 CORE_PEER_LOCALMSPID="wdpMSP" CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ic.org/peers/peer0.wdp.org/tls/ca.crt peer channel update -o orderer.kabukid.org:7050 -c $CHANNEL_NAME -f ./channel-artifacts/wdpMSPidentitiesanchors.tx --tls --cafile $ORDERER_CA
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ic.org/users/Admin@ic.org/msp CORE_PEER_ADDRESS=peer0.ic.org:7051 CORE_PEER_LOCALMSPID="icMSP" CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/ic.org/peers/peer0.ic.org/tls/ca.crt peer channel update -o orderer.kabukid.org:7050 -c $CHANNEL_NAME -f ./channel-artifacts/icMSPidentitiesanchors.tx --tls --cafile $ORDERER_CA
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank.org/users/Admin@bank.org/msp CORE_PEER_ADDRESS=peer0.bank.org:7051 CORE_PEER_LOCALMSPID="bankMSP" CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank.org/peers/peer0.bank.org/tls/ca.crt peer channel update -o orderer.kabukid.org:7050 -c $CHANNEL_NAME -f ./channel-artifacts/bankMSPidentitiesanchors.tx --tls --cafile $ORDERER_CA
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/kabukid.org/users/Admin@kabukid.org/msp CORE_PEER_ADDRESS=peer0.kabukid.org:7051 CORE_PEER_LOCALMSPID="kabukidMSP" CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/kabukid.org/peers/peer0.kabukid.org/tls/ca.crt peer channel update -o orderer.kabukid.org:7050 -c $CHANNEL_NAME -f ./channel-artifacts/kabukidMSPidentitiesanchors.tx --tls --cafile $ORDERER_CA
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/lgu.org/users/Admin@lgu.org/msp CORE_PEER_ADDRESS=peer0.lgu.org:7051 CORE_PEER_LOCALMSPID="lguMSP" CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/lgu.org/peers/peer0.lgu.org/tls/ca.crt peer channel update -o orderer.kabukid.org:7050 -c $CHANNEL_NAME -f ./channel-artifacts/lguMSPidentitiesanchors.tx --tls --cafile $ORDERER_CA
