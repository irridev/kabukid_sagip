#!/bin/bash

#create channel-artifacts directory
mkdir channel-artifacts

#generate crypto-materials
cryptogen generate --config=./crypto-config.yaml
#generate genesis block
configtxgen -profile OrdererGenesis -outputBlock ./channel-artifacts/genesis.block

#create channels
export CHANNEL_NAME=channela && configtxgen -profile $CHANNEL_NAME -outputCreateChannelTx ./channel-artifacts/$CHANNEL_NAME.tx -channelID $CHANNEL_NAME

#set anchor peers
export CHANNEL_NAME=channela && configtxgen -profile $CHANNEL_NAME -outputAnchorPeersUpdate ./channel-artifacts/wdpMSPidentitiesanchors.tx -channelID $CHANNEL_NAME -asOrg wdpMSP
export CHANNEL_NAME=channela && configtxgen -profile $CHANNEL_NAME -outputAnchorPeersUpdate ./channel-artifacts/icMSPidentitiesanchors.tx -channelID $CHANNEL_NAME -asOrg icMSP
export CHANNEL_NAME=channela && configtxgen -profile $CHANNEL_NAME -outputAnchorPeersUpdate ./channel-artifacts/bankMSPidentitiesanchors.tx -channelID $CHANNEL_NAME -asOrg bankMSP
export CHANNEL_NAME=channela && configtxgen -profile $CHANNEL_NAME -outputAnchorPeersUpdate ./channel-artifacts/kabukidMSPidentitiesanchors.tx -channelID $CHANNEL_NAME -asOrg kabukidMSP
export CHANNEL_NAME=channela && configtxgen -profile $CHANNEL_NAME -outputAnchorPeersUpdate ./channel-artifacts/lguMSPidentitiesanchors.tx -channelID $CHANNEL_NAME -asOrg lguMSP
