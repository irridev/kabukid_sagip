import React, {Component} from 'react'
import {
    StyleSheet,
    ImageBackground,
    FlatList,
    View
} from 'react-native'
import PropTypes from 'prop-types'
import ClaimItem from './ClaimItem'



export default class ClaimsList extends Component {
    static navigationOptions = {
        title: 'Claims',
        headerStyle: {
            backgroundColor: '#667746',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        }
    }
    

    constructor() {
        super()
        const claims = [
            {
                id : 1,
                farmerID: 100,
                farmerName: 'Carl Acampado',
                amount: 80000,
                region: 4,
                city: 'Los Baños',
                date: '08/08/08',
                status: 'active'
            },
            {
                id : 2,
                farmerID: 32,
                farmerName: 'Jek delos Santos',
                amount: 20000,
                region: 4,
                city: 'Calamba',
                date: '01/17/99',
                status: 'inProgress'
            }
        ]
        
        this.state = {
            claims
        }
    }


    render() {
        const { claims } = this.state
        return (
            <ImageBackground source = {require('../resources/rice_field.jpg')} style={styles.container}>
                <FlatList style={styles.container} 
                    data={claims}
                    renderItem={({item, index})=> (
                        <ClaimItem 
                            id={index}
                            claim={item}
                            onSelect={() => this.props.navigation.navigate('Timeline')}
                        />
                    )}
                />
            </ImageBackground>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    }
})