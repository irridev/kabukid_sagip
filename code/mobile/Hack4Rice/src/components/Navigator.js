import {createStackNavigator, createAppContainer} from 'react-navigation'
import ClaimsList from './ClaimsList'
import Timeline from './Timeline'

const Navigator = createStackNavigator({
    Claims: {screen: ClaimsList},
    Timeline: {screen: Timeline},
})

const App = createAppContainer(Navigator)

export default App