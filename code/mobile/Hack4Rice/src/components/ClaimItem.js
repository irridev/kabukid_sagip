import React from 'react'
import {
    StyleSheet,
    View,
    Text,
    TouchableHighlight
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'


const ClaimItem = ({claim, onSelect=f=>f }) => {
    const statusColor = { color: 'lightgrey' }
    const status = claim.status
    
    if (status === 'active') {
        statusColor.color = 'orange'
    } else if (status === 'inProgress') {
        statusColor.color = 'yellow'
    } else if (status === 'completed') {
        statusColor.color = 'green'
    }

    return (
        <TouchableHighlight
            style={styles.button}
            onPress={()=>onSelect(claim.farmerID)}
            underlayColor='#2a3419' >
            <View style={styles.row}>
                <Icon style={styles.icon}
                    name={'user-circle'} 
                    size={40}
                />
                <Text style={styles.text}>{ claim.farmerName }</Text>
                <View style={[styles.status, statusColor.color]} />
            </View>
        </TouchableHighlight>
    )
}


const styles = StyleSheet.create({
    button: {
        borderBottomWidth: 0.5,
        borderBottomColor: 'lightgrey',
        height: 90,
        alignSelf: 'stretch',
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    icon: {
        margin: 20
    },
    status: {
        height: 20,
        width: 20,
        borderRadius: 10,
        margin: 5,
        backgroundColor: 'lightgrey',
        right: 5
    },
    text: {
        fontSize: 30,
        margin: 5,
    }
})

export default ClaimItem