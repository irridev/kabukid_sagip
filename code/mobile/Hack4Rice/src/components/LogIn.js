import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    Text,
    ImageBackground
} from 'react-native'
import PropTypes from 'prop-types'

export default class LogIn extends Component {
    constructor(props) {
        super(props)

        this.state = {
            user: '',
            password: ''
        }

        this.submit = this.submit.bind(this)
    }
    
    submit() {
        this.props.changeComponent(this.state.user.toLowerCase())
        this.setState({user: '', password: ''})
    }

    render() {
        return (
            <ImageBackground source = {require('../resources/rice_field.jpg')} style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                <View style={styles.container}>
                    <TextInput 
                        style={styles.textInput} 
                        placeholder='Username' 
                        onChangeText={(user) => this.setState({user})}
                        value={this.state.user}
                    />

                    <TouchableOpacity onPress={this.submit} style={styles.button}>
                        <Text style={{color: 'white'}}>Log In</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        )
    }
}

LogIn.propTypes = {
    changeComponent: PropTypes.func.isRequired
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    textInput: {
        margin: 12,
        paddingLeft: 6,
        fontSize: 16,
        backgroundColor: 'rgba(211,211,211,0.5)',
        height: 40,
        flexWrap: 'wrap'
    },
    button: {
        margin: 12,
        backgroundColor: '#1b5a42',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        color: 'white',
        fontSize: 20,
        height: 50
    }
})