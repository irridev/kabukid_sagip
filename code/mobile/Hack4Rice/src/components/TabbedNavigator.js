import * as React from 'react'
import { StyleSheet, Dimensions } from 'react-native'
import { createBottomTabNavigator, createAppContainer } from 'react-navigation'
import Navigator from './Navigator'
import WeatherIndexList from './WeatherIndexList'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

const TabNavigator = createBottomTabNavigator({
    Claims: {
        screen: Navigator,
        navigationOptions:{  
            tabBarIcon: ({tintColor}) => (
                <Icon 
                    name={'cash-multiple'}
                    color={tintColor}
                    size={30} 
                />
            )
        }  
    },
    Weather: {
        screen: WeatherIndexList,
        tabBarIcon: ({tintColor}) => (
            <Icon 
                name={'weather-pouring'}
                color={tintColor}
                size={30} 
            />
        )
    },
},
{
    initialRouteName: 'Claims',
    tabBarOptions: {
        activeTintColor: 'white',
        inactiveBackgroundColor: '#76865a',
        inactiveTintColor: '#556832',
        showLabel: true,
        showIcon: true,
        style: {
            backgroundColor: '#667746'
        }
    }
}
)

export default createAppContainer(TabNavigator)