import React, {Component} from 'react'
import {
    View,
    StyleSheet
} from 'react-native'
import LogIn from './LogIn'
import Timeline from './Timeline'
import Navigator from './Navigator'
import TabbedNavigator from './TabbedNavigator'

class Main extends Component {
    constructor() {
        super() 
        this.state = {
            componentSelected: '',
        }

        this.renderComponent = this.renderComponent.bind(this)
    }

    componentDidMount() {
        this.setState({
            componentSelected: 'login'    
        })
    }

    changeComponent = (component) => {
        this.setState({
            componentSelected: component    
        })
    }

    
    renderComponent = (component) => {
        if(component === 'lgu') {
            return <Navigator changeComponent={this.changeComponent} />      
        } else if(component === 'agency') {
            return <Navigator changeComponent={this.changeComponent} />
        } else if(component === 'farmer') {
            return <Timeline changeComponent={this.changeComponent} />
        }  else if(component === 'bank') {
            return <Navigator changeComponent={this.changeComponent} />
        }  else if(component === 'kabukid') {
            return <TabbedNavigator changeComponent={this.changeComponent} />
        } 
            
        return <LogIn changeComponent={this.changeComponent}/>
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderComponent(this.state.componentSelected)}
            </View> 
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
})

export default Main