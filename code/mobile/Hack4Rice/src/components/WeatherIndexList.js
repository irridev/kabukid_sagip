import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    ImageBackground,
    Text,
    Image
} from 'react-native'
import PropTypes from 'prop-types'



export default class WeatherIndexList extends Component {
    static navigationOptions = {
        title: 'Weather Index'
    }

    render() {
        return (
            <ImageBackground source = {require('../resources/rice_field.jpg')} style={styles.container}>
                <View>
                    <Text>WeatherIndexList</Text>
                </View>
            </ImageBackground>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    }
})