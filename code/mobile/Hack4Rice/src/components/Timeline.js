import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    Image,
    Text
} from 'react-native'
import PropTypes from 'prop-types'
import TimelineCmpt from 'react-native-timeline-flatlist'


export default class Timeline extends Component {
    static navigationOptions = {
        title: 'Claim Timeline',
        headerStyle: {
            backgroundColor: '#667746',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        }
    }

    constructor(){
        super()
        this.data = [
            {
                time: '09/01/19', 
                title: 'Processing Claim', 
                description: 'His farm was affected by the recent typhoon. Insurance payout will now be processed.', 
                circleColor: '#009688',
                lineColor: '#009688'
            },
            {
                time: '09/01/19', 
                title: 'Tranferring Funds', 
                description: 'Transferring funds from insurance.',                
                circleColor: '#009688',
                lineColor: '#009688'
            },
            {
                time: '09/01/19', 
                title: 'Claim Granted', 
                description: 'Insurance claim has been approved.',                
                circleColor: '#009688',
                lineColor: '#009688'
            },
        ]
    }

    render() {
        return(
            <View style={styles.container}>
                <Image style={styles.stretch} source={require('../resources/field.jpeg')}/>
                <Image style={styles.profile} source={require('../resources/farmer.jpg')}/>
                <Text style={styles.name}>Kabukid Farmer</Text>
                <TimelineCmpt
                    style={styles.timeline}
                    data={this.data}
                    descriptionStyle={{ color: 'gray' }}
                />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignContent: 'flex-end'
    },
    stretch: {
        flex: 3,
        width: '100%',
        resizeMode: 'cover'
    },
    name: {
        fontSize: 20,
        fontWeight: 'bold',
        top: -90,
        marginLeft: 130
    },
    timeline: {
        flex: 2,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignContent: 'center',
        marginLeft: 100,
        top: -60
    },
    profile: {
        width: 100,
        height: 100,
        borderRadius: 50,
        marginLeft: 20,
        top: -50
    }
})